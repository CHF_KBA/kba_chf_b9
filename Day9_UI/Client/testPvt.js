const { clientApplication } = require('./client');

let dealerClient = new clientApplication();
const transientData = {
    make: Buffer.from('Honda'),
    model: Buffer.from('Accord'),
    color: Buffer.from('White'),
    dealerName: Buffer.from('XXX')
}

dealerClient.generateAndSubmitTxn(
    "dealer",
    "Admin",
    "autochannel",
    "KBA-Automobile",
    "OrderContract",
    "privateTxn",
    transientData,
    "createOrder",
    "Order-06"
).then(msg => {
    console.log(msg.toString())
});
