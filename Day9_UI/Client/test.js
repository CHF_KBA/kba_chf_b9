const { clientApplication } = require('./client')

let ManufacturerClient = new clientApplication();

ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin", //User1
    "autochannel",
    "KBA-Automobile",
    "CarContract",
    "invokeTxn",
    "",
    "createCar",
    "Car121",
    "Hatchback",
    "Nexon",
    "Red",
    "21/07/2021",
    "1"
).then(message => {
    console.log(message.toString());
})

