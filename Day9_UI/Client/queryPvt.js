const { clientApplication } = require('./client')

let dealerClient = new clientApplication()

dealerClient.generateAndSubmitTxn(
    "dealer",
    "Admin",
    "autochannel",
    "KBA-Automobile",
    "OrderContract",
    "queryTxn",
    "",
    "readOrder",
    "Order-06"
).then(message => {
    console.log(message.toString())
})
