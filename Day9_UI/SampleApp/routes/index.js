var express = require('express');
var router = express.Router();
const { clientApplication } = require('../../Client/client')
let ManufacturerClient = new clientApplication();
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Manufacturer Dashboard' });
});

/*Add to BlockChain*/
router.post('/createCar', function(req,res){
  const carId=req.body.CarId
  const make=req.body.CarMake
  const model=req.body.CarModel
  const color=req.body.CarColor
  const dom=req.body.DOM
  const manufacturer=req.body.ManufacturerName

  ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin", 
    "autochannel",
    "KBA-Automobile",
    "CarContract",
    "invokeTxn",
    "",
    "createCar",
    carId,make,model,color,dom,manufacturer
    ).then(message => {
      console.log(message.toString());
      res.status(200).send({message:"Added Car"})
  })
  .catch(error => {
    console.log("Some error Occured: ", error)
    res.status(500).send({ error: `Failed to Add`, message: `${error}` })
  })
})

router.post('/readCar', async function (req, res) {
  const readCarId = req.body.CarId;

  ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin",
    "autochannel",
    "KBA-Automobile",
    "CarContract",
    "queryTxn",
    "",
    "readCar",
    readCarId
  ).then(message => {
    console.log(message.toString());
    res.send({ Cardata: message.toString() });
  });

})




module.exports = router;

